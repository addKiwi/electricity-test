/**
 * This class is just a facade for your implementation, the tests below are using the `World` class only.
 * Feel free to add the data and behavior, but don't change the public interface.
 */

 import { Household } from "./Household.js"
 import { PowerPlant } from "./PowerPlant.js"
 
 export class World {
   constructor() {
    this.powerPlants = [];
    this.houseHolds = [];
   }
 
   createPowerPlant() {
     const powerPlant = new PowerPlant();

     this.powerPlants.push(powerPlant);
 
     return powerPlant;
   }
 
   createHousehold() {
     const household = new Household();

     this.houseHolds.push(household);
 
     return household;
   }
 
   connectHouseholdToPowerPlant(household, powerPlant) {
     household.connectPowerPlant(powerPlant);
   }
 
   connectHouseholdToHousehold(household1, household2) {
     household1.connectHouseHold(household2);
   }
 
   disconnectHouseholdFromPowerPlant(household, powerPlant) {
     household.disconnectPowerPlant(powerPlant);
   }
 
   killPowerPlant(powerPlant) {
     powerPlant.killPowerPlant();
 
     if(!this.powerPlants.some(plant => plant.isAlive)) {
       this.houseHolds.forEach(house => house.hasElectricity = false);
     }
   }
 
   repairPowerPlant(powerPlant) {
     powerPlant.repair();
   }
 
   householdHasEletricity(household) {
     return household.hasElectricity;
   }
 }
 
