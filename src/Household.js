export class Household {
  constructor() {
    this.name = 'houseHold';
    this.id = Date.now()
    this.hasElectricity = false;
    this.connectedPowerPlants = [];
    this.connectedHouseHolds = [];
  }

  connectPowerPlant(powerPlant) {
    const hasConnection = this.connectedPowerPlants.includes(powerPlant);
  
      if (!hasConnection) {
        this.connectedPowerPlants.push(powerPlant);
        powerPlant.connectHouseHold(this);
        this.updateElectricity();
      }
  }

  connectHouseHold(household) {
    const hasConnection = this.connectedHouseHolds.includes(household) || this === household;
      
    if(!hasConnection) {
      this.connectedHouseHolds.push(household);
      household.connectHouseHold(this);
      this.updateElectricity();
    }
  }

  disconnectPowerPlant(powerPlant) {
    this.connectedPowerPlants = this.connectedPowerPlants.filter(plant => plant !== powerPlant)

    this.updateElectricity();
  }

  updateElectricity() {
    if(!this.connectedHouseHolds.length && !this.connectedPowerPlants.length) {
      this.hasElectricity = false;
      return;
    }

    const prevState = this.hasElectricity;

    this.hasElectricity = this.connectedHouseHolds.some(house => house.hasElectricity);

    this.hasElectricity = this.hasElectricity 
      || this.connectedPowerPlants.some(plant => plant.isAlive);

    if(prevState !== this.hasElectricity) {
      this.connectedHouseHolds.forEach(house => house.updateElectricity());
    }
  }
}
