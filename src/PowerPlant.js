export class PowerPlant {
  constructor() {
    this.isAlive = true;
    this.connectedHouseholds = [];
  }

  connectHouseHold(household) {
    this.connectedHouseholds.push(household)
  }

  killPowerPlant() {
    this.isAlive = false;
    this.updateHouseHolds();
  }

  repair() {
    this.isAlive = true;
    this.updateHouseHolds();
  }

  updateHouseHolds() {
    this.connectedHouseholds.forEach(house => house.updateElectricity());
  }
}
